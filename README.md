# Gitea Italia
[Gitea.it](https://gitea.it) è la comunità di collaborazione italiana senza scopo di lucro per progetti liberi e Open Source. 

Gitea.it è stata fondata dall'organizzazione non-profit [devol](https://devol.it) con l'obiettivo di dare al codice Open-Source che gestisce il nostro mondo una posto sicuro e amichevole, e per garantire che il codice libero rimanga libero e sicuro per sempre.

Ti invitiamo ad unirti a noi come membro attivo o sostenitore, al fine di plasmare e garantire il futuro dello sviluppo Open-Source.

## Iscrizione a Gitea.it 
Contattaci su Mastodon: [@devol@mastodon.uno](https://mastodon.uno/@devol) per avere un account.

## La missione
Lo sviluppo del Software Libero e Open Source sta vivendo un boom ininterrotto, dovuto alla disponibilità generale di internet e agli effetti di rete sociale che ne derivano, moltiplicando la comunicazione, lo scambio di idee e la produttività ogni mese. Il numero di sviluppatori e progetti che partecipano al movimento Open-Source sta crescendo esponenzialmente. Solo i nuovi strumenti software e le piattaforme di collaborazione hanno reso queste dinamiche possibili e gestibili.

Mentre tutti gli strumenti software di successo che hanno permesso questo sviluppo sono stati forniti dalla comunità del Software Libero e Open Source, le piattaforme commerciali a scopo di lucro dominano l'hosting dei risultati del nostro lavoro collaborativo. Questo ha portato al paradosso che letteralmente milioni di volontari creano, raccolgono e mantengono conoscenze, documentazione e software inestimabili, per alimentare piattaforme chiuse guidate da interessi commerciali, il cui programma non è visibile né controllabile dall'esterno. Considerando il destino di startup di successo come Github, SourceForge, dobbiamo rompere il cerchio ed evitare che la storia si ripeta.

La missione di Gitea italia è di costruire e mantenere una piattaforma di collaborazione libera per creare, archiviare e preservare il codice e documentare il suo processo di sviluppo.

Le dipendenze da servizi commerciali, esterni o proprietari per il funzionamento della piattaforma sono quindi decisamente evitate, al fine di garantire indipendenza e affidabilità.

Gitea Italia è organizzata come un'organizzazione idealista senza scopo di lucro, non come una startup speculativa che spera nella prossima uscita da un miliardo di dollari. Questo assicura la nostra attenzione alla stabilità e alla sostenibilità a lungo termine, ma implica anche che noi, la comunità, siamo responsabili del nostro futuro.

Vogliamo dare il nostro meglio e contiamo sul vostro sostegno per costruire questo futuro nel modo migliore che possiamo immaginare. Unisciti a noi e sostieni lo sviluppo del software libero e open source unendoti a Gitea. come membro attivo e sostenitore, o donando alla nostra causa.

L'istanza italiana di Gitea è gestita dal [collettivo devol](https://devol.it), il servizio è mantenuto nei loro server, potete [sostenerli con una piccola donazione](https://ko-fi.com/devol) 

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/G2G82N2NN)

E' POSSIBILE DONARE ANCHE CON CRIPTOVALUTE  

Usando il browser Brave, potete attivare gli annunci delle ricompense e [donare parte delle criptomonete BAT a Gitea.it](https://brave.com/brave-rewards/).
Accettiamo anche donazioni con altre criptomonete:

[![coin](https://coindrop.to/embed-button.png)](https://coindrop.to/gitea)


## Cosa c'è dopo
La prima versione di Gitea.it funziona ora con risorse piuttosto limitate. Monitoreremo attentamente la nostra crescita e ci assicureremo di estendere l'infrastruttura dove sensato e necessario, ma di farlo in modo responsabile e senza sprecare i fondi affidati.

Abbiamo anche idee per nuovi servizi che potremmo offrire ai progetti di software libero - vi terremo aggiornati attraverso il collettivo devol che è possibile seguire su [mastodon](https://mastodon.uno/@devol), su [Matrix](https://chat.mozilla.org/#/room/#mastodon:mozilla.org) e con la [newsletter](https://buttondown.email/devol). Se volete lavorare con noi in questo settore, non esitate a contattarci.

## Avviso
Stiamo iniziando in piccolo e con mezzi limitati. Faremo del nostro meglio per garantire la disponibilità di codeberg.org. Per favore segnalate tutti i problemi che si verificano.

## Perché usare Gitea, l'alternativa etica a Gitlab e Github

Vale la pena ricordare che Github e Gitlab distribuiscono entrambi il software dei loro servizi come software libero. A meno che non si dica altrimenti, questo post riguarda il loro servizio, non il loro software.

### Perchè non usare Gitlab

Il "software libero" che obbliga all'esecuzione di software non libero non è veramente libero. 

* Non c'è niente di particolarmente sbagliato nel software di gitlab, ma quel software deve essere ospitato e configurato e ci sono molti problemi etici con il servizio [gitlab.com](http://gitlab.com):
* Trattamento sessista nei confronti delle venditrici a cui [viene detto di indossare](https://web.archive.org/web/20200309145121/https://www.theregister.co.uk/2020/02/06/gitlab_sales_women/) abiti, tacchi, ecc.
* Ospitato da server Google.
* [Proxy](https://about.gitlab.com/blog/2020/01/16/gitlab-changes-to-cloudflare/) attraverso il servizio CloudFlare che è una minaccia per la privacy (man in the middle).
* [tracking](https://social.privacytools.io/@darylsun/103015834654172174)
* Trattamento ostile degli utenti Tor che cercano di registrarsi.
* Trattamento ostile dei nuovi utenti che tentano di registrarsi con un indirizzo email di inoltro @spamgourmet.com per tracciare lo spam e per proteggere il loro indirizzo email interno più sensibile.
* Trattamento ostile degli utenti Tor dopo che hanno creato un account e hanno dimostrato di essere un non-spammer.

Per quanto riguarda l'ultimo punto, stavo semplicemente cercando di modificare un messaggio esistente che avevo già postato e sono stato costretto a risolvere un CAPTCHA. Ci sono diversi problemi con questo:

* CAPTCHA Serve a bloccare i bot e i bot non sono necessariamente maligni. Ad esempio, avrei potuto avere un bot che correggeva un errore di ortografia diffuso in tutti i miei messaggi.
* I CAPTCHA mettono gli esseri umani al lavoro per le macchine quando sono le macchine che dovrebbero funzionare per gli esseri umani.
* I CAPTCHA sono sconfitti. Gli spammer trovano economico usare la manodopera del terzo mondo per i CAPTCHA mentre gli utenti legittimi hanno questo fardello di CAPTCHA rotti.
* Il puzzle reCAPTCHA richiede una connessione a Google
* 1. I reCAPTCHA di Google compromettono la sicurezza come conseguenza del capitalismo della sorveglianza che comporta la raccolta di indirizzi IP, la stampa del browser.
* 1. * l'anonimato è [compromesso](https://cryptome.org/2016/07/cloudflare-de-anons-tor.htm).
* 1. 1. * (speculativo) Google potrebbe spingere j/s maligni che intercettano le informazioni di registrazione degli utenti?
* 2. Gli utenti sono costretti ad eseguire [javascript non-free](https://libreplanet.org/wiki/Group:Free_Javascript_Action_Team#Ideas_for_focus) ([recaptcha/api.js](https://www.google.com/recaptcha/api.js)).
* 3. reCAPTCHA richiede un'interfaccia grafica, negando così il servizio agli utenti di clienti testuali.
* 4. I CAPTCHA mettono gli esseri umani al lavoro per le macchine quando sono le macchine a dover lavorare per gli esseri umani. PRISM corp Google Inc. beneficia finanziariamente del lavoro di risoluzione dei puzzle, dando a Google l'opportunità di raccogliere dati, abusarne e trarne profitto. Ad esempio, Google può tenere traccia di quali dei suoi utenti registrati stanno visitando la pagina di presentazione del CAPTCHA.
* 5. I CAPTCHA sono spesso rotti. Ciò equivale a una negazione del servizio. ![gitlab_google_recaptcha](https://user-images.githubusercontent.com/18015852/51769530-9d494300-20e3-11e9-9830-1610b3ae9059.png)
*    
* 5. * Il server CAPTCHA stesso si rifiuta di dare il puzzle dicendo che c'è troppa attività.
* 5. * E.g.2: ![](https://user-images.githubusercontent.com/18015852/55681364-07713600-5926-11e9-8874-137e4faaf423.png)
* 6. I CAPTCHA sono spesso irrisolvibili.
* 6. * Es.1: il puzzle CAPTCHA è rotto dall'ambiguità (un pixel in una cella della griglia di un palo che tiene un cartello stradale è considerato un cartello stradale?)
* 6. 7. * Es.2: il puzzle è espresso in una lingua che l'utente non capisce.
* 7. * (nota: per un breve momento [gitlab.com](http://gitlab.com/) è passato a hCAPTCHA di Intuition Machines, Inc. ma ora sono tornati al reCAPTCHA di Google)
* 8. 8. * Abuso della neutralità della rete: c'è una disuguaglianza di accesso per cui gli utenti che hanno effettuato l'accesso agli account di Google ricevono un trattamento più favorevole [trattamento](https://www.fastcompany.com/90369697/googles-new-recaptcha-has-a-dark-sideby) del CAPTCHA (ma poi assumono più abusi della privacy). Gli utenti di Tor ricevono un trattamento più duro.

Il motivo per il reCAPTCHA che viene ospitato su Google.com è la condivisione dei cookie. Questo permette a reCAPTCHA di ottenere maggiori informazioni su ciò che si affida a Google online...

Per questo motivo gitlab.com dovrebbe essere elencato come servizio da evitare, come MS [Github](https://github.com).


### Perchè non usare GitHub

Questo non è direttamente collegato al repository youtube-dl che è stato tolto da GitHub da RIAA a causa di una [richiesta DMCA](https://github.com/ytdl-org/youtube-dl/).

Non è una novità che [Microsoft abbia acquistato GitHub nel 2018](https://it.wikipedia.org/wiki/GitHub), lo sanno tutti. Eppure, nonostante questo, migliaia dei più importanti progetti Open Source del mondo continuano ad ospitare il loro codice su GitHub. La gente sembra aver dimenticato quanto sia realmente marcia Microsoft e quanto sia pericolosa questa situazione.

Github era proprietario fin dall'inizio, ma dopo l'acquisizione di Microsoft si è spostato più in là nella fase Estenti (EEE Embrace, Extend, extinguish) con Github CLI. Stanno puntando a far orbitare intorno a loro l'intera infrastruttura open source. Non aiutateli a spostare il mondo del software libero e dell'open source in questa direzione. In realtà non avete bisogno di Github. Loro hanno bisogno di voi.

Non è tanto il fatto che molti progetti ospitano i loro lavori su GitHub, quanto il fatto che molti progetti non hanno messo al sicuro il codice al di fuori di GitHub! Si affidano completamente a GitHub per mantenere e proteggere il codice.

Microsoft sta acquistando molto attivamente importanti progetti legati all'Open Source e nell'aprile 2020 è stato annunciato anche l'acquisito di [npm](https://it.wikipedia.org/wiki/Npm_(software)), un fornitore di packaging JavaScript, per una somma di denaro non rivelata.

Forse le giovani generazioni non sanno nulla dei "mali" passati di Microsoft e credono ingenuamente che Microsoft sia ora il buon amico dell'Open Source, ma la verità è che tutte queste acquisizioni di progetti Open Source sono una tattica di business che viene messa in atto per migliorare la posizione perdente di Microsoft nei confronti dell'Open Source. È una questione di controllo.

[Microsoft ha annunciato](https://www.minecraft.net/en-us/article/java-edition-moving-house) che Minecraft richiederà un account Microsoft per giocare nel 2021 e che i proprietari della versione classica saranno costretti a migrare.

Anche se questo non ha nulla a che fare con l'Open Source, è un ottimo esempio di quanto possa andare male se Microsoft in futuro deciderà che i progetti su GitHub dovranno fare qualcosa che va contro gli interessi di questi progetti.

Non farò nomi, perché questo non è importante, ma come può un progetto Open Source che considera la sua base di codice come preziosa non assicurarsi di avere una copia completamente aggiornata di ogni singola linea di codice al di fuori di GitHub!

Alcuni sviluppatori di progetti tengono solo parti del codice in repository personali, altri non hanno nemmeno un backup, ma si fidano pienamente del fatto che GitHub avrà sempre una versione funzionante e aggiornata dei latests commits.

Per anni la gente ha messo in guardia sulla posizione che GitHub ha avuto nel mondo dell'Open Source perché concentra troppo del potere in una singola entità. Avere Microsoft al volante rende la situazione mille volte peggiore.

Nessuno sano di mente avrebbe mai immaginato di caricare codice Open Source su server Microsoft solo una decina di anni fa. Microsoft è stata l'arcinemica dell'Open Source negli anni Novanta e ha messo in atto ogni tipo di tattica sporca per tenere gli altri sistemi operativi fuori dal mercato, specialmente le tattiche sporche contro Linux. Nei primi anni 2000 l'allora CEO Steve Ballmer disse: Linux è un cancro che si attacca in senso di proprietà intellettuale a tutto ciò che tocca. E per molti anni hanno cercato di ottenere il controllo di Linux e manipolato il mercato in diversi modi per "schiacciare la concorrenza". Quando si sono resi conto di non poterlo fare e che la battaglia era persa, hanno messo in atto una nuova tattica in cui invece cercano di fare soldi con Linux, che è quello che stanno facendo ora in molti settori, ed è per questo che sembrano "più amichevoli" alla comunità Open Source.

Io stesso ho un po' di codice che risiede su GitHub, ma naturalmente ho anche diversi cloni e backup aggiornati altrove. Tuttavia, avere il più grande repository al mondo di codice Open Source importante risiede nelle mani di Microsoft è semplicemente follia. Perché non sono migrati tutti i progetti più importanti? Gestire un server Git auto-ospitato non è così difficile ed esistono anche diverse soluzioni piuttosto solide.

Sempre più di tutto ciò che c'è di buono nello sviluppo e nella condivisione di risorse, codice ed esperienza in Open Source e nella comunità, sta lentamente venendo divorato o rovinato e massacrato dalle grandi aziende o dalle fondazioni basate sull'economia. Perché non appena il denaro entra in gioco così tante cose si trasformano in "merda"? Certo, l'avidità è la risposta, ma una domanda ancora più importante di questa è: Perché abbiamo smesso di preoccuparci?


#### Privacy problemi con Microsoft Github service

1. MS supporta utenti che abusano della privacy:
* 1. Github usa Amazon AWS che scatena diversi problemi di privacy e di etica
* 2. (2012) MS ha speso 35 milioni di dollari in pubblicità su Facebook, diventando così il terzo più alto sostenitore finanziario di un famigerato abusatore della privacy quell'anno.
2. 2. Censura e interferenza del progetto: Lo staff di Github ha apparentemente cancellato un collaboratore che segnalava un abuso della privacy presente in altri progetti. L'ostilità verso i sostenitori volontari della privacy è di per sé un motivo sufficiente per abbandonare Github.
3. Github potrebbe avere una politica che comporta la censura delle segnalazioni di bug (vedi questo post per la discussione)
4. Github è ostile a Tor (secondo il progetto Tor, anche se personalmente non ho avuto problemi ad usare Tor per il GH)
5. La MS è una società PRISM incline alla sorveglianza di massa
6. 6. La MS fa pressioni per una politica ostile alla privacy:
* 6. La MS ha sostenuto lo scambio di informazioni non giustificate della CISPA e della CISA e la CISA è stata approvata.
* 6. (2018) MS  ha pagato 195.000 dollari per combattere la privacy in CA
7. MS fornisce il servizio di ricerca Bing che dà un alto grado di ranking ai siti web CloudFlare che abusano della privacy.
8. MS fornisce il servizio di posta elettronica hotmail.com, che utilizza il vigilante estremista org Spamhaus per costringere gli utenti di Internet residenziali a condividere tutti i loro metadati e-mail e payloads con una terza parte aziendale.
9. 9. MS fa Drug test ai suoi dipendenti, invadendo così la loro privacy al di fuori del luogo di lavoro.
10. 10. I prodotti della MS (in particolare Office) violano il GDPR
11. Per segnalare un bug di sicurezza della MS, è necessario effettuare l'accesso e la pagina di accesso è rotta. È davvero un male per la sicurezza rendere le segnalazioni di difetti difficili da presentare.


#### Conseguenza dell'utilizzo di Github per un progetto che compromette la privacy:

1. (conflitto di interessi) seleziona solo i collaboratori disposti a scendere a compromessi sulla privacy, ed esclude coloro che non utilizzeranno GH per motivi di privacy.
2. 2. (conflitto di interessi) Quando i collaboratori valutano se uno strumento è rispettoso della privacy, mettono nella lista bianca Microsoft e Amazon come conseguenza dell'utilizzo di Github, e poi lo usano come motivazione per sostenere uno strumento indegno.
3. (effetto collaterale) I sostenitori della privacy che usano Github devono affrontare critiche demoralizzanti per quella che alcuni considerano un'ipocrisia. 

#### Motivazione per rimanere con Github:

Lo sconvolgimento per la migrazione farà perdere i collaboratori.

#### ALtri Problemi

Altre letture rilevanti: Cosa c'è di sbagliato nell'acquisto di GitHub da parte di Microsoft](https://jacquesmattheij.com/what-is-wrong-with-microsoft-buying-github/)

#### Alternative

I grandi progetti dovrebbero auto-ospitare i loro repository per rimanere completamente indipendenti, ma esistono alcune soluzioni alternative ai servizi più popolari come GitHub, GitLab e BitBucket (elenco non esaustivo):

[Gitea](gitea.it/) un fork di Gogs gestito dalla comunità - 
Gitea.it ospita molti progetti focalizzati sulla privacy. Gitea.it è un luogo dove gli sviluppatori con gli stessi obiettivi potrebbero svilupparsi in modo più unitario mentre i progetti focalizzati sulla privacy presenti su GH e GL hanno il problema dell'ipocrisia che affrontano utilizzando dei servii poco rispettosi della privacy.

[Codeberg](https://codeberg.org/)
Codeberg è un'organizzazione no-profit tedesca. Codeberg non dipende da servizi esterni. Nessun cookie di terze parti, nessun tracciamento. Ospitato nell'UE.

[NotABug](https://notabug.org/)
NotABug.org è gestito da Peers, un gruppo di persone interessate al software libero e alla società libera. Ma è soprattutto per piccoli progetti. 

[sourcehut](https://sourcehut.org/)
sourcehut è attualmente considerato alfa e non rimarrà libero, ma non ha alcun tracciamento o pubblicità. Tutte le funzioni funzionano senza JavaScript. Informativa sulla privacy. Discussione pertinente su Hacker News. Dopo la registrazione si riceve il seguente messaggio: Il pagamento è facoltativo durante l'alfa, ma sappiate che diventerà obbligatorio in seguito. Questo servizio è finanziato dai suoi utenti, non dagli investitori.

[Gogs](https://gogs.io/) - 

[OneDev](https://github.com/theonedev/onedev) - 

